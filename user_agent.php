<?php

class UserAgent
{

    public $platform;
    public $browser;
    public $user_agent;

    function __construct(){

        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];

        $this->platform = $this->platform( $this->user_agent );
        $this->browser  = $this->browser( $this->user_agent );
    }
    function platform($user_agent) { 

        if( !$user_agent ) return;

        $os_platform    =   "Unknown OS Platform";

        $platforms       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

        foreach ( $platforms as $regex => $value ) { 

            if ( preg_match( $regex, $user_agent ) ) {
                $os_platform    =   $value;
            }
        }   
        return $os_platform;
    }

    function browser( $user_agent ) {  

        if( !$user_agent ) return;

        $browser        =   "Unknown Browser";

        $browser_array  =   array(
                                '/Trident/i'    =>  'Internet Explorer', // IE 11
                                '/msie/i'       =>  'Internet Explorer',
                                '/firefox/i'    =>  'Firefox',
                                '/chrome/i'     =>  'Chrome',
                                '/edge/i'       =>  'Edge',
                                '/opera/i'      =>  'Opera',
                                '/netscape/i'   =>  'Netscape',
                                '/maxthon/i'    =>  'Maxthon',
                                '/konqueror/i'  =>  'Konqueror',
                                '/safari/i'     =>  'Safari',
                                '/Edge/i'       =>  'Microsoft Edge', // ok
                                '/mobile/i'     =>  'Handheld Browser',
                            );

        foreach ( $browser_array as $regex => $value) { 

            if ( preg_match( $regex, $user_agent ) ) {
                if( $value == 'Handheld Browser' && $browser != 'Unknown Browser' )
                    $browser    .=   ', ' . $value;
                else
                    $browser     = $value;
            }
        }
        return $browser;
    }
}

$ua = new UserAgent();

echo 'Your Platform => ' . $ua->platform . '<br>';
echo 'Your Browaer  => ' . $ua->browser . '<br>';
echo 'User Agent    => ' . $ua->user_agent . '<br>';


